module.exports = function(Widget) {

  var app = require('../../server/server')

  Widget.observe('after save', function(ctx, next){
    app.mqttHelper.updateWidgets();
    next();
  });

  Widget.observe('after delete', function(ctx, next){
    app.mqttHelper.updateWidgets();
    next();
  });

  Widget.publishToTopic = function(topic, field, dataType, value, cb) {
    if(field != null && field != "") {
      var message = {};
      console.log(value);
      if(value != undefined && value != null && value != "") {
        message[field] = app.mqttHelper.mapToDataType(dataType, value);
        console.log(JSON.stringify(message));
      } else {
        message[field] = "";
        console.log(JSON.stringify(message));
      }
      console.log(JSON.stringify(message));
      app.mqttHelper.publish(topic, JSON.stringify(message));
    } else {
      app.mqttHelper.publish(topic, value);
    }
    cb(null, null);
  }

  Widget.remoteMethod('publishToTopic', {
    accepts: [
      {arg: 'topic', type: 'string', http: {source: 'query'}},
      {arg: 'field', type: 'string', http: {source: 'query'}},
      {arg: 'dataType', type: 'string', http: {source: 'query'}},
      {arg: 'value', type: 'string', http: {source: 'query'}}
    ]
  });
};
