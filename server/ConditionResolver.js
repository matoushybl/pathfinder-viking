module.exports = function ConditionResolver() {

  this.isStringConditionMet = function(input, operator, compareValue) {
    if(typeof(input) != "string" || typeof(compareValue) != "string") {
      console.log("input or compareValue not a string");
      return false;
    }

    switch(operator) {
      case "==":
        return input == compareValue;
      break;
      case "!=":
        return input != compareValue;
      break;
    }

    return false;
  }

  this.isNumberConditionMet = function(input, operator, compareValue) {
    if(isNaN(input) || isNaN(compareValue)) {
      console.log("input not a number or compare value is not a number");
      return false;
    }

    const number = Number(compareValue);

    switch(operator) {
      case "==":
        return input == number;
      break;
      case "!=":
        return input != number;
      break;
      case ">":
        return input > number;
      break;
      case ">=":
        return input >= number;
      break;
      case "<":
        return input < number;
      break;
      case "<=":
        return input <= number;
      break;
    }

    return false;
  }

  this.isBooleanConditionMet = function(input, operator, compareValue) {
    const value = (compareValue == "true");
    if(typeof(input) != "boolean") {
      console.log("input not a boolean");
      return false;
    }

    switch(operator) {
      case "==":
        return input == value;
      break;
      case "!=":
        return input != value;
      break;
    }

    return false;
  }
}
