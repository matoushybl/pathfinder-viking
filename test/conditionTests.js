const assert = require('chai').assert;
const ConditionResolver = require('../server/ConditionResolver.js')

describe("Conditions", () => {
  const conditionResolver = new ConditionResolver();

  describe("StringConditions", () => {
      it("should equal when values are the same", () => {
        assert.equal(conditionResolver.isStringConditionMet("hello", "==", "hello"), true);
      });
      it("should not equal when values are not the same", () => {
        assert.equal(conditionResolver.isStringConditionMet("hello there", "==", "hello"), false);
      });
      it("should equal when values are not the same", () => {
        assert.equal(conditionResolver.isStringConditionMet("hello there", "!=", "hello"), true);
      });
      it("should not equal when the values are the same", () => {
        assert.equal(conditionResolver.isStringConditionMet("hello", "!=", "hello"), false);
      });
      it("should return false when incompatible type passed", () => {
        assert.equal(conditionResolver.isStringConditionMet(30, "!=", "hello"), false);
      });
  });

  describe("NumberConditions", () => {
    it("should return true when one number equals to another", () => {
      assert.equal(conditionResolver.isNumberConditionMet(30, "==", "30"), true);
    });
    it("should return false when one number does not equal to another", () => {
      assert.equal(conditionResolver.isNumberConditionMet(30, "==", "35"), false);
    });
    it("should return true when one number higher than another", () => {
      assert.equal(conditionResolver.isNumberConditionMet(35, ">", "30"), true);
    });
    // FIXME write more
  });

  describe("BooleanConditions", () => {
    it("should return true when true equals to true", () => {
      assert.equal(conditionResolver.isBooleanConditionMet(true, "==", "true"), true);
    })
    it("should return false when true equals to false", () => {
      assert.equal(conditionResolver.isBooleanConditionMet(true, "==", "false"), false);
    })
    it("should return true when false equals to true", () => {
      assert.equal(conditionResolver.isBooleanConditionMet(false, "!=", "true"), true);
    })
    it("should return false when true doesn't to true", () => {
      assert.equal(conditionResolver.isBooleanConditionMet(true, "!=", "true"), false);
    })
  });
});
