module.exports = function(Logger) {
  Logger.observe('after save', function(ctx, next){
    app.mqttHelper.updateLoggers();
    next();
  });

  Logger.observe('after delete', function(ctx, next){
    app.mqttHelper.updateLoggers();
    next();
  });
};
