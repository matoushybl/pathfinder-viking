module.exports = function(Rule) {

  var app = require('../../server/server')

  Rule.observe('after save', function(ctx, next){
    app.mqttHelper.updateRules();
    next();
  });

  Rule.observe('after delete', function(ctx, next){
    app.mqttHelper.updateRules();
    next();
  });

  Rule.maker = function(name, body, callback) {
    app.mqttHelper.handleMessage(name, JSON.stringify(body));
    callback(null, null);
  }

  Rule.remoteMethod('maker', {
    accepts: [
      {arg: 'name', type: 'string', http: {source: 'query'}},
      {arg: 'body', type: 'object', http: {source: 'body'}}
    ]
  });

};
