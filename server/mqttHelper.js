module.exports = function MqttHelper() {

  const mqtt = require('mqtt');
  const http = require('http');
  const app = require('./server');
  const ConditionResolver = require('./ConditionResolver.js');

  this.mqttClient = mqtt.connect('mqtt://localhost');
  const mqttClient = this.mqttClient;
  const conditionResolver = new ConditionResolver();

  var rules = [];
  var widgets = [];
  var loggers = [];

  this.publish = function(topic, message) {
    if(topic.startsWith("::maker")) {
      topic = topic.substr(8, topic.length);
      var params = topic.split(":");
      // skip first one
      for(var i = 1; i < params.length; i++) {
        if(params[i].startsWith("{") && params[i].endsWith("}")) {
          params[i] = params[i].replace("{", "");
          params[i] = params[i].replace("}", "");
          params[i] = String(this.createJSONIfNeeded(message)[params[i]]);
        }
      }
      http.request(`http://maker.ifttt.com/trigger/${params[0]}/with/key/cpa93R1_SVH3SWDnccTKd4?value1=${params[1]}&value2=${params[2]}&value3=${params[3]}`).end();
    } else {
      mqttClient.publish(topic, message);
    }
  }

  this.init = function() {
    mqttClient.on('message', (topic, message) => {
      this.handleMessage(topic, message);

      widgets.filter(widget => widget.inputTopic == topic)
             .forEach(widget => {
                if(widget.inputField != null && widget.inputField != "") {
                  try {
                    widget.lastValue = JSON.parse(message)[widget.inputField].toString();
                  } catch(e) {
                    return;
                  }
                } else {
                  widget.lastValue = message;
                }
                widget.save();
      });

      loggers.filter(logger => logger.topic == topic)
             .forEach(logger => {
                if(this.isNotEmptyString(logger.property)) {
                  try {
                    var json = JSON.parse(message);
                    if(typeof(json[logger.property]) == logger.dataType) {
                      var record = {};
                      record.time = new Date();
                      record.value = json[logger.property].toString();
                      logger.records.create(record);
                    }
                  } catch(e) {return;}
                } else {
                  if(typeof(message) == logger.dataType) {
                    var record = {};
                    record.time = new Date();
                    record.value = message.toString();
                    logger.records.create(record);
                  }
                }
             });

      if(topic == "device/status") {
        var status = JSON.parse(message);
        // Find or create is not working
        app.models.Device.findOne({where: {deviceId: status.deviceId}},
          (error, device) => {
            if(device == null) {
              device = new app.models.Device()
            }
            if(status.deviceId != undefined && status.deviceId != null && status.deviceId != "") {
              device.deviceId = status.deviceId;
            }
            device.type = status.type;
            device.status = status.status;
            device.deviceTypeTopic = status.deviceTypeTopic;
            device.automatic = true;
            device.save();
        });
      }

    });

    mqttClient.on('connect', () => {
      mqttClient.subscribe("device/status");
      this.updateRules();
      this.updateWidgets();
      this.updateLoggers();
    });
  }

  this.updateRules = function() {
    rules.forEach(rule => mqttClient.unsubscribe(rule.inputTopic));

    app.models.Rule.find({where: { enabled: true}}, (error, newRules) => {
      rules = newRules;
      rules.map(rule => rule.inputTopic)
           .filter(topic => !topic.startsWith("::maker"))
           .forEach(topic => mqttClient.subscribe(topic));
    });
  }

  this.updateWidgets = function() {
    widgets.map(widget => widget.inputTopic)
           .filter(this.isNotEmptyString)
           .forEach(topic => mqttClient.unsubscribe(topic));

    app.models.Widget.find((error, newWidgets) => {
        widgets = newWidgets;
        widgets.map(widget => widget.inputTopic)
               .filter(this.isNotEmptyString)
               .forEach(topic => mqttClient.subscribe(topic));
    });
  }

  this.updateLoggers = function() {
    loggers.map(logger => logger.topic)
           .filter(this.isNotEmptyString)
           .forEach (topic => mqttClient.unsubscribe(topic));
    app.models.Logger.find((error, newLoggers) => {
      loggers = newLoggers;
      loggers.map(logger => logger.topic)
           .filter(this.isNotEmptyString)
           .forEach (topic => mqttClient.subscribe(topic));
    });
  }

  this.handleMessage = function(topic, message) {
    rules.filter(rule => rule.inputTopic == topic)
         .forEach(rule => {
            const condition = rule.condition;
            const mapping = rule.mapping;
            const rewrite = rule.rewrite;
            if(condition != null) {
              if(this.isConditionMet(condition, message)) {
                if(mapping == null) {
                  if(rewrite != null) {
                    message = this.rewriteMessage(rewrite, message);
                  }
                  this.publish(rule.outputTopic, message);
                } else {
                  this.publish(rule.outputTopic, this.mapMessage(mapping, message));
                }
              }
            } else if(mapping != null) {
              this.publish(rule.outputTopic, this.mapMessage(mapping, message));
            } else {
              if(rewrite != null) {
                message = this.rewriteMessage(rewrite, message);
              }
              this.publish(rule.outputTopic, message);
            }
    });
  }

  this.isConditionMet = function(condition, message) {
  // FIXME check for validity of JSON
    if(condition == null || message == null) {
      return false;
    }
    const messageJSON = JSON.parse(message);
    const inputValue = messageJSON[condition.sourceProperty];
    if(inputValue != null) {
      switch(condition.sourceType) {
        case "string":
          return conditionResolver.isStringConditionMet(inputValue, condition.operator,
             condition.compareValue)
        case "number":
          return conditionResolver.isNumberConditionMet(inputValue, condition.operator,
            condition.compareValue)
        case "boolean":
          return conditionResolver.isBooleanConditionMet(inputValue, condition.operator,
            condition.compareValue)
      }
    }
  }

  this.mapMessage = function(mapping, message) {
    // FIXME check for validity
    if(message == "") {
      return message;
    }
    const messageJSON = JSON.parse(message);
    var inputValue = messageJSON[mapping.inputField];
    var outputValue = mapping.trueValue;

    // FIXME add operatorless mapping in the future - mostly toString
    switch(mapping.inputType) {
      case "number":
        if(!conditionResolver.isNumberConditionMet(inputValue, mapping.condition, mapping.compareValue)) {
          outputValue = mapping.falseValue;
        }
      break;
      case "string":
        if(!conditionResolver.isStringConditionMet(inputValue, mapping.condition, mapping.compareValue)) {
          outputValue = mapping.falseValue;
        }
      break;
      case "boolean":
        if(!conditionResolver.isBooleanConditionMet(inputValue, mapping.condition, mapping.compareValue)) {
          outputValue = mapping.falseValue;
        }
      break;
    }
    outputValue = this.mapToDataType(mapping.outputType, outputValue);
    messageJSON[mapping.outputField] = outputValue;
    return JSON.stringify(messageJSON);
  }

  this.mapToDataType = function(dataType, value) {
    switch(dataType) {
      case "number":
        return Number(value);
      break;
      case "string":
        return value;
      break;
      case "boolean":
        return (value == "true") ? true : false;
      break;
    }
    return "";
  }

  this.createJSONIfNeeded = function(data) {
    try {
      return JSON.parse(data);
    } catch(e) {
      return {};
    }
  }

  this.rewriteMessage = function(rewrite, message) {
      var jsonMessage = this.createJSONIfNeeded(message);
      jsonMessage[rewrite.property] = this.mapToDataType(rewrite.dataType, rewrite.value);
      return JSON.stringify(jsonMessage);
  }

  this.isNotEmptyString = function(value) {
    return value != "" && value != null && value != undefined
  }
}
